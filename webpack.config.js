const path = require('path');
const glob = require('glob');

module.exports = {
  mode: 'development',
  context: path.join(__dirname, 'src'),
  entry: glob.sync('src/**/*.ts').reduce((acc, path) => {
    const entry = path.replace("src", ".").replace(".ts", "")
    console.log(entry)
    acc[entry] = path.replace("src", ".")
    return acc
  }, {}),
  output: {
    path: path.join(__dirname, 'dist'),
    libraryTarget: 'commonjs',
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'babel-loader',
      },
    ],
  },
  target: 'web',
  externals: /^k6(\/.*)?/,
  stats: {
    colors: true,
  },
};
