{
  initPipeline(maxVusCount):: {
    services: ["docker:dind"],
    default: {image: 'ackee/gitlab-builder'},
    stages: ['build app'] + [std.format("benchmark with %d users", x) for x in std.range(100, maxVusCount) if x % 100 == 0],
    variables: {
      GCP_SA_KEY: "$SECRET_GCP_SA_KEY",
      GCP_SA_KEY_JSON_PATH: "/tmp/key.json",
      RUN: "MaxVUSCountTestsCI"
    },
    "Build app": {
      stage: "build app",
      image: "node:14.16.0",
      script: [
        'npm install',
        'npx webpack'
      ],
      artifacts: {paths: ["./dist"], expire_in: "1 day"}
    }
  },
  benchMarkJob(vusCount): {
    stage: std.format("benchmark with %d users", vusCount),
    variables: {
      VUS_COUNT: std.format("%d", vusCount)
    },
    script: [
      'docker-compose rm -f',
      'echo "$GCP_SA_KEY" | base64 -d > $GCP_SA_KEY_JSON_PATH',
      'gcloud auth activate-service-account --key-file=$GCP_SA_KEY_JSON_PATH',
      'export GOOGLE_APPLICATION_CREDENTIALS=$GCP_SA_KEY_JSON_PATH',
      'export GCP_AUTH_TOKEN=`gcloud auth application-default print-access-token`',
      'docker-compose up',
      'docker-compose down -v',
      'docker-compose rm -f'
    ],
    retry: 2,
    rules: [{'if': '$RUN == "MaxVUSCountTestsCI"'}],
    artifacts: {paths: ["results/.passed_jobs"], expire_in: "1 day"}
  },
}
