#!/bin/bash

set -eu -o pipefail

export RUN=${RUN:-LoadTests}

systctl_commands=(
    "fs.inotify.max_user_watches=524288"
    "net.ipv4.tcp_tw_reuse=1"
    'net.ipv4.ip_local_port_range="1024 65535"'
    "net.ipv4.tcp_timestamps=1"
)

for i in "${systctl_commands[@]}"; do
    if ! sysctl -w "$i"; then
        echo "Could not enable $i"
    fi
done
if ! sysctl -p; then
    echo "Failed to list sysctl -p"
fi
ulimit -n 250000

case $RUN in

    MaxVUSCountTestsCI)
        [ ! -f /results/.passed_jobs ] && ls /src/src/tests/ > /results/.passed_jobs
        echo -n > .passed_jobs_new_iteration
        cat /results/.passed_jobs | while read i; do
            i=`basename $i`
            if k6 run \
                -e GCP_AUTH_TOKEN="${GCP_AUTH_TOKEN}" \
                -e VUS=$VUS_COUNT \
                --compatibility-mode=base \
                /src/dist/tests/${i%.*}.js; then
                    echo $i >> .passed_jobs_new_iteration;
            fi
        done
        cat .passed_jobs_new_iteration
        cat .passed_jobs_new_iteration > /results/.passed_jobs
    ;;

    *)
    echo -n "unknown run command"
    ;;
esac

