local jobs = import 'lib/jobs.libsonnet';

local maxVusCount = std.parseInt(std.extVar('MAX_VUS_COUNT'));

std.manifestYamlDoc(
    jobs.initPipeline(maxVusCount)
    +
    {
        [std.format("Stage Benchmark %d", i)]:
            jobs.benchMarkJob(i) for i in std.range(100, maxVusCount) if i % 100 == 0
    },
    indent_array_in_object=false
)
