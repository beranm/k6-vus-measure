import http from 'k6/http';

const GCPProject: string = __ENV.GCPPROJECT ?? "terraform-test-hejda";

export default (value: any, labels: any, type: string = "custom.googleapis.com/benchmars/frontend-load-test") => {
    const GCP_AUTH_TOKEN: string = __ENV.GCP_AUTH_TOKEN ?? ""
    if (GCP_AUTH_TOKEN === ""){
        console.log("GCP_AUTH_TOKEN missing, not uploading results");
        return;
    }
    const d = new Date();
    const toSend = {
        "timeSeries": [
            {
            "metric": {
                "type": type,
                "labels": labels
            },
            "resource": {
                "type": "global",
                "labels": {
                "project_id": GCPProject
                }
            },
            "metricKind": "GAUGE",
            "points": [
                {
                    "interval": {
                        "endTime": d.toISOString()
                    },
                    "value": {
                        "doubleValue": value
                    }
                }
            ]
            }
        ]
    }
    const response = http.post(
        `https://monitoring.googleapis.com/v3/projects/${GCPProject}/timeSeries`,
        JSON.stringify(toSend),
        {
            headers: {
                'Authorization' : `Bearer ${GCP_AUTH_TOKEN}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=utf-8',
            },
        }
    );
    console.log(response.body);
    if ('error' in response && response.error.length){
        console.log(response.error);
    }
}