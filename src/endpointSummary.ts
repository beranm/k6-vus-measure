import uploadResultsToStackDriver from './uploadResultsToStackDriver';
import { textSummary } from './k6/summary';

export default (data: any, endpoint: string, tags: Record<string, string>, type: string = "custom.googleapis.com/benchmars/frontend-load-test") => {
    const threshold_pass = String(data.metrics.checks?.thresholds?.['rate>0.99']?.ok ?? "Not Measured")
    Object.keys(data.metrics.group_duration.values).forEach(function (key) {
        const labels = {
            threshold_pass,
            "endpoint_name": endpoint,
            "value_type": key
        };
        if (data.metrics.grpc_req_duration)
            uploadResultsToStackDriver(data.metrics.grpc_req_duration.values[key], labels, type);
    });
    Object.values(data.root_group.groups).forEach(function (item: any) {
        const labels = {
            threshold_pass,
            "endpoint_name": endpoint,
            "group_name": item.name,
        };
        if (item.checks.length){
            const fails = item.checks[0]['fails'];
            const passes = item.checks[0]['passes'];
            uploadResultsToStackDriver(fails, {...labels, value_type: 'fails'}, type);
            uploadResultsToStackDriver(passes,{...labels, value_type: 'passes'}, type);
            uploadResultsToStackDriver((fails / (fails + passes)) * 100, {...labels, value_type: 'fails_percent'}, type);
            uploadResultsToStackDriver((passes / (fails + passes)) * 100,{...labels, value_type: 'passes_percent'}, type);
        }
    });
    const fails = data.metrics.checks.values.fails;
    const passes = data.metrics.checks.values.passes;
    uploadResultsToStackDriver((fails / (fails + passes)) * 100, {"endpoint_name": endpoint, "value_type": "fails_percent", "threshold_pass": threshold_pass}, type);
    uploadResultsToStackDriver((passes / (fails + passes)) * 100, {"endpoint_name": endpoint, "value_type": "passes_percent", "threshold_pass": threshold_pass}, type);
    uploadResultsToStackDriver(data.metrics.checks.values.fails, {"endpoint_name": endpoint, "value_type": "fails", "threshold_pass": threshold_pass}, type);
    uploadResultsToStackDriver(data.metrics.checks.values.passes, {"endpoint_name": endpoint, "value_type": "passes", "threshold_pass": threshold_pass}, type);
    let iterations_count: string = "0";
    if ('iterations' in data.metrics)
        iterations_count = data.metrics.iterations.values.count;
    uploadResultsToStackDriver(iterations_count, {"endpoint_name": endpoint, "value_type": "iterations", "threshold_pass": threshold_pass}, type);
    uploadResultsToStackDriver(data.metrics.vus_max.values.value, {"endpoint_name": endpoint, "value_type": "vus_max", "threshold_pass": threshold_pass}, type);
    const retObj: any = {};
    retObj[`/results/${endpoint}-summary.json`] = JSON.stringify(
        {
            "metrics": data,
            "tags": tags,
        }, null, 2
    );
    retObj[`/results/${endpoint}-StackDriver.json`] = JSON.stringify(
        {
            "metrics": data.metrics.grpc_req_duration,
            "tags": tags,
        }, null, 2
    );
    retObj['stdout'] = textSummary(data, { indent: ' ', enableColors: true});
    return retObj;
}
