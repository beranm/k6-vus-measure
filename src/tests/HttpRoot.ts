import { Options } from "k6/options";
import endpointSummary from '../endpointSummary';
import { group, check } from "k6";
import http from 'k6/http';

const endpointName = "HttpRoot";
export const duration: string = __ENV.DURATION ?? "60s";
export const vus: number = parseInt(__ENV?.VUS ?? "1000");

const TAGS = {
    scenario: "benchmark",
    rpc: endpointName,
}

export const options: Options = {
    userAgent: "AckeeK6Bot",
    scenarios:
    {
        benchmark: {
            duration,
            vus,
            executor: 'constant-vus',
            tags: TAGS,
            gracefulStop: duration,
        },
    },
    thresholds: {
        checks: ['rate>0.99'],
    },
    insecureSkipTLSVerify: true,
}

export default () => {
    group('load test', () => {
            const response = http.get(
                `https://www.ackee.cz/`,
                {
                    redirects: 0,
                }
            );
            check(response, {
                'status is OK': (r) => r && Number(r.status) === 200,
            });
        }
    );
};

export function handleSummary (data: any) {
    return endpointSummary(
        data, endpointName, TAGS
    );
}
