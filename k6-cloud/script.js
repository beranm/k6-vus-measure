import { group, check } from "k6";
import http from 'k6/http';

export let options = {
    userAgent: "AckeeK6Bot",
    scenarios:
    {
        benchmark: {
            startVUs: 0,
            executor: 'ramping-vus',
            gracefulStop: '60s',
            stages: [
                {duration: '0s', target: 0},
                {duration: '50s', target: 50},
            ],
        },
    },
    thresholds: {
        checks: ['rate>0.99'],
    },
    insecureSkipTLSVerify: true,
    ext: {
        loadimpact: {
            projectID: 3558638,
            name: "Ackee Frontpage Test"
        }
    }
}

export default () => {
    group('load test', () => {
            const response = http.get(
                `https://www.ackee.cz/`,
                {
                    redirects: 0,
                }
            );
            check(response, {
                'status is OK': (r) => r && Number(r.status) === 200,
            });
        }
    );
};