import { group, check } from "k6";
import http from 'k6/http';

export let options = {
    scenarios:
    {
        benchmark: {
            VUs: 100,
            executor: 'constant-vus',
            duration: '60s',
        },
    },
}

export default () => {
    group('load test', () => {
            const response = http.get(`https://www.ackee.cz/`);
            check(response, {
                'status is OK': (r) => r && Number(r.status) === 200,
            });
        }
    );
};