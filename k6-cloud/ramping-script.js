import { group, check } from "k6";
import http from 'k6/http';

export let options = {
    scenarios:
    {
        benchmark: {
            startVUs: 0,
            executor: 'ramping-vus',
            gracefulStop: '60s',
            stages: [
                {duration: '0s', target: 0},
                {duration: '60s', target: 60},
            ],
        },
    },
}

export default () => {
    group('load test', () => {
            const response = http.get(`https://www.ackee.cz/`);
            check(response, {
                'status is OK': (r) => r && Number(r.status) === 200,
            });
        }
    );
};